# MIT License

# Copyright (c) 2019 kogiokka

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import re
import sys


def oswalk_deco(func):
    def wrapper(*args):
        subdirs = [os.path.abspath(x[0]) for x in os.walk(target_dir)]
        for dir in subdirs:
            os.chdir(dir)
            print(os.getcwd())
            for item in os.listdir("."):
                p = re.match(".*\.svg", item)
                if isinstance(p, re.Match):
                    match = p.group()
                    func(match, *args)
        os.chdir(target_dir)

    return wrapper


@oswalk_deco
def docname_format(filename):
    with open(filename, "r") as file:
        content = file.read()
    with open(filename, "w") as file:
        content = re.sub(r'docname=".*"', f'docname="{filename}"', content)
        file.write(content)


def uni2han(filename):
    match = re.match("u.*\.", filename)
    if isinstance(match, re.Match):
        return f"\\u{match.group()[1:-1]}".encode("utf8").decode("unicode_escape")
    else:
        return filename


@oswalk_deco
def sub_str(filename, replace_list, dotall=False):
    with open(filename, "r") as file:
        content = file.read()
    with open(filename, "w") as file:
        for pair in replace_list:
            if not isinstance(pair, tuple):
                sys.exit("replace_list can only contain tuples.")
            if dotall is True:
                content = re.sub(pair[0], pair[1], content, flags=re.DOTALL)
            else:
                content = re.sub(pair[0], pair[1], content)
        file.write(content)


@oswalk_deco
def regex_print(filename, search, dotall=False, count=False):
    with open(filename, "r") as file:
        content = file.read()
    if dotall is True:
        relist = re.findall(search, content, re.DOTALL)
    else:
        relist = re.findall(search, content)

    if count is True:
        relist = len(relist)
    print({uni2han(filename): relist})


@oswalk_deco
def color_palette(filename):
    with open(filename, "r") as file:
        content = file.read()
        raw_lG = re.findall(
            '<linearGradient\n *id=".*"\n *osb:paint="[a-z]*">', content
        )
        raw_stop = re.findall(
            '<stop\n *style=".*"\n *offset="[0-9]*"\n *id=".*" />', content
        )
        raw_lG_ID = []
        for id in raw_lG:
            url = re.findall('id=".*"', id)[0][4:-1]
            fillurl = re.findall(f"fill:url\(#{url}\)", content)
            if fillurl != []:
                raw_lG_ID = raw_lG_ID + fillurl
            else:
                raw_lG_ID.append("THIS IS NOT SUPPOSED TO MATCH ANY STRING")

        stop_color = [re.findall("#[a-z0-9]{6}", b) for b in raw_stop]
        lG = []
        lG_ID = []
        stop = []
        for t in list(zip(raw_lG, stop_color, raw_stop, raw_lG_ID)):
            if t[1][0] == "#00988c":
                lG_ID.append((t[3], "fill:url(#lG01)"))
                lG.append((t[0], re.sub('id=".*"', 'id="lG01"', t[0])))
                stop.append((t[2], re.sub('id=".*"', 'id="lG01-stop"', t[2])))
            elif t[1][0] == "#90dd90":
                lG_ID.append((t[3], "fill:url(#lG02)"))
                lG.append((t[0], re.sub('id=".*"', 'id="lG02"', t[0])))
                stop.append((t[2], re.sub('id=".*"', 'id="lG02-stop"', t[2])))
            elif t[1][0] == "#ff9955":
                lG_ID.append((t[3], "fill:url(#lG03)"))
                lG.append((t[0], re.sub('id=".*"', 'id="lG03"', t[0])))
                stop.append((t[2], re.sub('id=".*"', 'id="lG03-stop"', t[2])))
            elif t[1][0] == "#d35f5f":
                lG_ID.append((t[3], "fill:url(#lG04)"))
                lG.append((t[0], re.sub('id=".*"', 'id="lG04"', t[0])))
                stop.append((t[2], re.sub('id=".*"', 'id="lG04-stop"', t[2])))
    with open(filename, "w") as file:
        for i in lG:
            content = re.sub(i[0], i[1], content)
        for j in stop:
            content = re.sub(j[0], j[1], content)
        for k in lG_ID:
            content = content.replace(k[0], k[1])
        file.write(content)


def check_palette(a="#00988c", b="#90dd90", c="#ff9955", d="#d35f5f", e="#b3b3b3"):
    regex_print(
        """\n  <defs
     id="colors">
    <linearGradient
       id="lG01"
       osb:paint="solid">
      <stop
         style="stop-color:"""
        + a
        + """;stop-opacity:1;"
         offset="0"
         id="lG01-stop" />
    </linearGradient>
    <linearGradient
       id="lG02"
       osb:paint="solid">
      <stop
         style="stop-color:"""
        + b
        + """;stop-opacity:1;"
         offset="0"
         id="lG02-stop" />
    </linearGradient>
    <linearGradient
       id="lG03"
       osb:paint="solid">
      <stop
         style="stop-color:"""
        + c
        + """;stop-opacity:1;"
         offset="0"
         id="lG03-stop" />
    </linearGradient>
    <linearGradient
       id="lG04"
       osb:paint="solid">
      <stop
         style="stop-color:"""
        + d
        + """;stop-opacity:1;"
         offset="0"
         id="lG04-stop" />
    </linearGradient>
    <linearGradient
       id="lG05"
       osb:paint="solid">
      <stop
         style="stop-color:"""
        + e
        + """;stop-opacity:1;"
         offset="0"
         id="lG05-stop" />
    </linearGradient>
  </defs>""",
        True,
        True,
    )


if __name__ == "__main__":
    if len(sys.argv) == 1:
        target_dir = "."
    else:
        target_dir = sys.argv[1]

    target_dir = os.path.abspath(target_dir)
    # docname_format()
    # sub_str([
    # 	('stroke-[a-z]*:[.0-9a-zA-Z]*;?', ''),
    # 	('\n *gradientTransform=".*"', ''),
    # ])
    # regex_print('  <title\n     id="title">Array Input Method</title>')
    # regex_print('stroke-[a-z]*:[.0-9a-zA-Z]*;?')
    # regex_print('\n *gradientTransform=".*"')
    # regex_print('docname=".*"')
    # regex_print('<path', False, True)
    # check_palette()
